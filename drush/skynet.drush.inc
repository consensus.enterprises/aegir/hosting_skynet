<?php
/**
 * @file Provides the Skynet Drush hooks.
 */

require_once(dirname(__FILE__) . '/includes/templates.inc');

/**
 * Implements drush_hook_post_COMMAND().
 *
 * Whenever we migrate the hostmaster site, we need to re-write the skynet
 * config file with the new DB credentials, and restart the service.
 */
function drush_skynet_post_provision_verify() {
  if (!_skynet_is_enabled()) return;
  _skynet_symlink_script();
  _skynet_create_config();
  drush_log('Ensuring that Hosting Queue Daemon is disabled.');
  _skynet_disable_hosting_queued();
  drush_log('Ensuring that Cron task queue is disabled.');
  _skynet_disable_cron_task_queue();
}

/**
 * Determine whether we're operating in the Aegir site context, and that Skynet
 * is enabled.
 */
function _skynet_is_enabled() {
  if (d()->type != 'site') return FALSE;
  if (d()->profile != 'hostmaster') return FALSE;
  if (!d()->site_enabled) return FALSE;
  $features = drush_get_option('hosting_features', ['skynet' => 0]);
  return (boolean) $features['skynet'];
}

/**
 * Implements drush_HOOK_post_COMMAND().
 */
function drush_skynet_post_provision_migrate() {
  drush_skynet_post_provision_verify();
}

/**
 * Implements drush_HOOK_post_COMMAND().
 *
 * When we install the hostmaster site, we need to write the skynet config file
 * with the DB credentials, and start the service.
 */
function drush_skynet_post_provision_install() {
  if (d()->type == 'site' && d()->profile == 'hostmaster') {
    drush_skynet_post_provision_verify();
  }
}

/**
 * Helper function to symlink to the skynet script from a stable location.
 */
function _skynet_symlink_script() {
  $source_path = dirname(__FILE__) . '/skynet.py';
  $dest_path = '/var/aegir/config/skynet/skynet.py';
  if (!is_dir(dirname($dest_path))) {
    drush_mkdir(dirname($dest_path));
  }
  $current_target = @readlink($dest_path); # silence warning when symlink doesn't yet exist.
  if ($current_target !== FALSE) { # symlink already exists.
    unlink($dest_path);
  }
  if ($current_target != $dest_path) { # create/update the symlink. 
    symlink($source_path, $dest_path);
  }
}

/**
 * Helper function to write the skynet config file.
 */
function _skynet_create_config() {
  $template_path = dirname(__FILE__) . '/templates/skynet.conf.tpl.php'; 
  $dest_path = '/var/aegir/config/skynet/skynet.conf';
  $site_context = drush_get_context('site');
  $variables = array(
    'host' => $site_context['db_host'],
    'db' => $site_context['db_name'], 
    'user' => $site_context['db_user'], 
    'passwd' => $site_context['db_passwd'],     
  );
  $mode = 0600;
  $overwrite = TRUE;
  skynet_template($template_path, $dest_path, $variables, $mode, $overwrite);
}

/**
 * Disable the default Hosting Queue Daemon, now that Skynet is running.
 */
function _skynet_disable_hosting_queued() {
  provision_backend_invoke('@hostmaster', 'pm-disable', ['hosting_queued'], ['no-verify' => 1, 'strict' => 0]);
}

/**
 * Disable the default cron task queue, now that Skynet is running.
 */
function _skynet_disable_cron_task_queue() {
  provision_backend_invoke('@hostmaster', 'variable-set', ['hosting_queue_tasks_enabled', '0'], ['no-verify' => 1, 'strict' => 0]);
}
