#!/usr/bin/python3

import signal
import subprocess

from cement import App, CaughtSignal, Controller, ex
from time import sleep
from MySQLdb import connect

class SkynetBase(Controller):
    class Meta:
        label = 'base'
        description = "Skynet is an experimental replacement for Aegir's queue daemon."

        arguments = [
            ( ['-v', '--verbose'],
                { 'help': 'Increase application verbosity.',
                  'action': 'store_true', } ),
        ]

    @ex(
        help="Run a queue daemon.",
        aliases=['q'],
        arguments = [
            ( ['-c', '--config_file'],
              { 'help': 'The path to the config file to use.',
                'action': 'store', } ),
        ]
    )
    def queued(self):
        self.app.log.debug("Inside SkynetBase.queued() function.")
        self.app.log.info("Waiting for next task.")
        self.load_config()
        if self.app.pargs.verbose:
            for slug in app.config.keys('database'):
                self.app.log.info("Database option '%s' = %s" % (slug, app.config.get('database',slug)))
            for slug in app.config.get_sections():
                self.app.log.info("Section '%s'" % slug)

        while True:
            db = connect(host = app.config.get('database','host'),
                         user = app.config.get('database','user'),
                       passwd = app.config.get('database','passwd'),
                           db = app.config.get('database','db'))
            cur = db.cursor()
            cur.execute("SELECT t.nid\
                           FROM hosting_task t\
                     INNER JOIN node n\
                             ON t.vid = n.vid\
                          WHERE t.task_status = %s\
                       ORDER BY n.changed, n.nid ASC" % 0)
            for row in cur.fetchall() :
                self.app.log.info("Running task NID: '%s'" % row[0])
                subprocess.call('drush @hostmaster hosting-task %s --strict=0 --interactive=true' % row[0], shell=True)
                # Reload config, in case the Hostmaster credentials have changed.
                self.app.log.info("Reloading Skynet config.")
                self.load_config()
            sleep(1)

    def load_config(self):
        if self.app.pargs.config_file:
            self.app.log.debug("Using config file at '%s'." % \
                          self.app.pargs.config_file)
            app.config.parse_file(self.app.pargs.config_file)
        else:
            self.app.log.debug("Using default config file at '%s'." % \
                          '~/skynet.conf')
            app.config.parse_file('~/skynet.conf')

class Skynet(App):
    class Meta:
        label = 'skynet'
        handlers = [
            SkynetBase,
        ]

with Skynet() as app:
    try:
        app.run()
    except CaughtSignal as e:
        pass
